# README #

This project is a coding exercise for JS Frontend developers 

### What is this repository for? ###

* Please fork this repository and implement a proof-of-concept single page application using React.js and Redux/Flux
* v1.0.0

### How do I get set up? ###

* Checkout the project and execute the local server with the sintax:
	`$> node bin/server.js`
	
### Where do I start? ###

Please modify pages/index.html so that renders functionally similar to the following:

![https://image.ibb.co/gqHRKf/Group.png](https://image.ibb.co/gqHRKf/Group.png)

### Tips ###

* The json data can be retrieved at http://localhost:8080/data
* The built js should be under the main directory and referred in the page (app.js)
* Changing the ticket type affects the total price
* Changing the quantity affects the price
* Pressing the arrow submit data to http://localhost:8080/next, and shows the  confirmation message
* The post to /next should be like {"qnt":1,"type:"ticketName"}
 

### Development guidelines ###

* Please focus on:
- Components architecture (Nice structure)
- Npm packaging and dependency management
- Use of module bundler (browserify/webpack)
- Material design

### Who do I talk to? ###

* dev@trumin.com
